@extends('webpages.templates.master')

@section('title', $title)
@section('description', $description)
@section('imageog', $imageog)

@section('content')
    
    <!--Jumbotron-->
    <div class="jumbotron">
      <!-- Jumbotron Text -->
      <div class="jumbotron-text">
        <p>can you imagine there is a tool for</p>
        <p>forex trading that is almost riskless?</p>
        <p>you can gain every morning</p>

        <!-- Jumbotron Learn More Button -->
        <button type="button" class="learnmore-btn">
          <span class="text-btn"> Learn More </span>
        </button>
      </div>

      <!--Jumbotron Video-->
      <div class="jumbotron-vid">
        <div class="vid">
          <div class="play-button">
            <div class="btn-play"></div>
          </div>
        </div>
      </div>
    </div>
    <!-- End of Jumbotron -->

    <!-- What is Swap Arbitrage -->
    <div class="swap-arb">
      <div class="swap-title">what is swap arbitrage?</div>
      <div class="swap-desc">
        Swap arbitrage trades Swap Rates against each other. All Positions are
        at all times hedged against each other and there is no directional
        trading risk involved
      </div>

      <!--Three Icon-->
      <div class="tri-icon">
        <div class="tri-one">
          <div class="tri-one-icon"></div>
          <br />
          <div class="tri-one-txt">100% Hedged Position</div>
        </div>
        <div class="tri-two">
          <div class="tri-two-icon"></div>
          <br />
          <div class="tri-two-txt">no direction trading risk involved</div>
        </div>
        <div class="tri-tri">
          <div class="tri-tri-icon"></div>
          <br />
          <div class="tri-tri-txt">advanced monitoring system</div>
        </div>
      </div>

      <!--Try Demo Button-->
      <div>
        <button type="button" class="demo-btn">
          <span class="text-demo"> Try Demo </span>
        </button>
      </div>
    </div>

    <!-- Live Trading Card -->
    <div class="trading-card">
      <div class="card">
        <!-- Trading Title -->
        <div class="trading-title">live trading results</div>

        <!-- Trading Form -->
        <div class="trading-form">
          <form>
            <div class="form-group row">
              <label class="col-sm-4 trading-label">Starting Balance</label>
              <div class="col-sm-8">
                <input type="text" class="input-balance" />
              </div>
            </div>
            <br />
            <div class="form-group row">
              <label class="col-sm-4 trading-label">Monthly Gains</label>
              <div class="col-sm-8">
                <input type="password" class="input-gains" />
              </div>
            </div>
          </form>
        </div>

        <!-- Trading Button -->
        <div class="calculate">
          <button type="button" class="trading-btn">
            <span class="text-trading"> Calculate </span>
          </button>
        </div>

        <!-- Trading Result -->
        <div class="trading-result">
          <div class="res">
            <p>12 months</p>
            <p>-</p>
          </div>
          <div class="res">
            <p>24 months</p>
            <p>-</p>
          </div>
        </div>
      </div>
    </div>
    <!-- End of Trading Card -->

    <!-- Market Summary -->
    <div class="market-card">
      <div class="card">
        <!-- Trading Title -->
        <div class="market-title">Market Summary</div>
        <!-- Trading Option -->
        <div class="market-opt">
          <ul>
            <li>Indices</li>
            <li>Currencies</li>
            <li>Futures</li>
            <li>Chrypto</li>
            <li>Bonds</li>
          </ul>
        </div>
        <div class="graph"></div>
        <div class="graph-time">
          <ul>
            <li>1D</li>
            <li>1M</li>
            <li>3M</li>
            <li>1Y</li>
            <li>5Y</li>
            <li>All</li>
          </ul>
        </div>
        <div class="currency"></div>
        <div class="show-more">Show More</div>
      </div>
    </div>

    <!-- Reason Section -->
    <div class="reason">
      <div class="reason-title">Reasons you need to join us NOW!</div>
      <div class="reason-list">
        <div class="card-list">
          <div class="card-icon">
            <img src="./img/lowestrisk.svg" alt="" />
          </div>
          <div class="card-content">
            <p class="p-title">Lowest possible risk</p>
            <p class="p-desc">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda
              aut facere quaerat vel ipsa quis suscipit
            </p>
          </div>
        </div>
        <div class="card-list">
          <div class="card-icon">
            <img src="./img/consistent.svg" alt="" />
          </div>
          <div class="card-content">
            <p class="p-title">Ultra consistent result</p>
            <p class="p-desc">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda
              aut facere quaerat vel ipsa quis suscipit
            </p>
          </div>
        </div>
        <div class="card-list">
          <div class="card-icon">
            <img src="./img/direction.svg" alt="" />
          </div>
          <div class="card-content">
            <p class="p-title">No directional position</p>
            <p class="p-desc">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda
              aut facere quaerat vel ipsa quis suscipit
            </p>
          </div>
        </div>
        <div class="card-list">
          <div class="card-icon">
            <img src="./img/autopilot.svg" alt="" />
          </div>
          <div class="card-content">
            <p class="p-title">Autopilot passive income</p>
            <p class="p-desc">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda
              aut facere quaerat vel ipsa quis suscipit
            </p>
          </div>
        </div>
        <div class="card-list">
          <div class="card-icon">
            <img src="./img/ea.svg" alt="" />
          </div>
          <div class="card-content">
            <p class="p-title">Latest technology EA</p>
            <p class="p-desc">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda
              aut facere quaerat vel ipsa quis suscipit
            </p>
          </div>
        </div>
        <div class="card-list">
          <div class="card-icon">
            <img src="./img/flex task.svg" alt="" />
          </div>
          <div class="card-content">
            <p class="p-title">Flexible Plan</p>
            <p class="p-desc">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda
              aut facere quaerat vel ipsa quis suscipit
            </p>
          </div>
        </div>
      </div>
    </div>

    <!-- How it Works -->
    <div class="how-it-work">
      <div class="how-title">How it works?</div>
      <div class="step-1">
        <div class="how-img"></div>
        <div class="step">
          <p>Step 1</p>
          <ul>
            <li>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. quidem
              delectus commodi magni, nemo consectetur laborum.
            </li>
            <br />
            <li>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. quidem
              delectus commodi magni, nemo consectetur laborum.
            </li>
          </ul>
        </div>
      </div>
      <div class="step-2">
        <div class="step">
          <p>Step 2</p>
          <ul>
            <li>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. quidem
              delectus commodi magni, nemo consectetur laborum.
            </li>
            <br />
            <li>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. quidem
              delectus commodi magni, nemo consectetur laborum.
            </li>
          </ul>
        </div>
        <div class="how-img"></div>
      </div>
      <div class="step-3">
        <div class="how-img"></div>
        <div class="step">
          <p>Step 3</p>
          <ul>
            <li>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. quidem
              delectus commodi magni, nemo consectetur laborum.
            </li>
            <br />
            <li>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. quidem
              delectus commodi magni, nemo consectetur laborum.
            </li>
          </ul>
        </div>
      </div>
    </div>
    <!-- End of How it works -->

    <!-- Get it Now -->
    <div class="get-it-card">
      <div class="card">
        <div class="card-body">
          <div class="card-head">
            <p>Rp xx xxx</p>
            <button class="get-it-btn">Get it now</button>
          </div>
          <br />
          <hr />
          <div class="get-it-list">
            <ul>
              <li>The Swap Master Expert Advisors (Master and Slave Unit)</li>
              <li>The Swap Master User Guide with many Best Practice Tips</li>
              <li>Access to our Portfolio Research</li>
              <li>Free Build Updates</li>
              <li>Personalized 24/7 Support</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- End of Get it now -->

    <!-- Help Section -->
    <div class="help-section">
      <div class="help-item">
        <div class="help-icon">
          <img src="./img/help.svg" alt="" />
        </div>
        <div class="help-title">Orrbit Help</div>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam,
          purus sit amet luctus venenatis
        </p>
      </div>
      <div class="help-item">
        <div class="help-icon">
          <img src="./img/support.svg" alt="" />
        </div>
        <div class="help-title">Support</div>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam,
          purus sit amet luctus venenatis
        </p>
      </div>
      <div class="help-item">
        <div class="help-icon">
          <img src="./img/discussion.svg" alt="" />
        </div>
        <div class="help-title">Discussion Blog and Articles</div>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam,
          purus sit amet luctus venenatis
        </p>
      </div>
    </div>
    <!-- End of Help Section -->

    <!-- Testimonial -->
    <div class="testimoni">
      <div class="testimoni-head">this is what they said about orrbit</div>
      <div class="testimoni-list">
        <div class="testimoni-card">
          <div class="card">
            <div class="testimoni-img">
              <img src="./img/testimoni1.svg" alt="" />
            </div>
            <div class="testimoni-kanan">
              <p class="the-testimoni">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit ut
                aliquam, purus sit amet luctus venenatis, lectus magna
              </p>
              <br />
              <div class="testimoni-name">Matthew Young</div>
              <div class="testimoni-title">Legal Consultant, Indonesia</div>
            </div>
          </div>
        </div>
        <div class="testimoni-card">
          <div class="card">
            <div class="testimoni-img">
              <img src="./img/testimoni2.svg" alt="" />
            </div>
            <div class="testimoni-kanan">
              <p class="the-testimoni">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit ut
                aliquam, purus sit amet luctus venenatis, lectus magna
              </p>
              <br />
              <div class="testimoni-name">Zahir Namane</div>
              <div class="testimoni-title">Legal Consultant, Indonesia</div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- End of Testimonial -->
@endsection