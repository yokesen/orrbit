<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function pageIndex(){

        $title = "Orrbit - Overnight Arbitrage";
        $description = "Swap arbitrage trades Swap Rates against each other. All Positions are at all times hedged against each other and there is no directional trading risk involved";
        $imageog = url('/')."/webpages/images/LogoYokosenPojokKanan.png";
        return view('webpages.homepage',compact('title','description','imageog'));
    }

    public function pagePricing(){
        return view('webpages.pricing');
    }
}
